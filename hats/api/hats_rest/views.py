from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import LocationVO, Hat
from .encoders import HatListEncoder, HatDetailEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        return JsonResponse(
            Hat.objects.all(),
            encoder=HatListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        content["location"] = LocationVO.objects.get(pk=content["location"])
        location = Hat.objects.create(**content)
        return JsonResponse(
            location,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Hat.objects.get(pk=pk),
            encoder=HatDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Hat.objects.filter(pk=pk).update(**content)
        return JsonResponse(
            Hat.objects.get(pk=pk),
            encoder=HatDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hat.objects.get(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )