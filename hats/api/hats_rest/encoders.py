from common.json import ModelEncoder
from .models import LocationVO, Hat

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name"
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "picture_url",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }