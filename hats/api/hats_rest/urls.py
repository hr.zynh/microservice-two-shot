from django.contrib import admin
from django.urls import path

from .views import api_hats, api_hat

urlpatterns = [
    path('', api_hats, name="api_hats"),
    path('<int:pk>/', api_hat, name="api_hat")
]