from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    href = models.CharField(max_length=50)

    def get_api_url(self):
        return self.href

    def __str__(self):
        return f"LocationVO<{self.closet_name}>"


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.CharField(max_length=250)
    location = models.ForeignKey(LocationVO, on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"Hat<{self.fabric} {self.style}>"