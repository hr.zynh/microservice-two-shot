from django.contrib import admin
from django.urls import path

from .views import api_shoes, api_shoe

urlpatterns = [
    path('', api_shoes, name="api_shoes"),
    path('<int:pk>/', api_shoe, name="api_shoe")
]