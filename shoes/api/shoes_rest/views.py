from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import BinVO, Shoe
from .encoders import ShoeListEncoder, ShoeDetailEncoder

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        return JsonResponse(
            Shoe.objects.all(),
            encoder=ShoeListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        content["bin"] = BinVO.objects.get(pk=content["bin"])
        location = Shoe.objects.create(**content)
        return JsonResponse(
            location,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, pk):
    if request.method == "GET":
        return JsonResponse(
            Shoe.objects.get(pk=pk),
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Shoe.objects.filter(pk=pk).update(**content)
        return JsonResponse(
            Shoe.objects.get(pk=pk),
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Shoe.objects.get(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )