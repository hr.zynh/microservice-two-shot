from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    href = models.CharField(max_length=50)

    def get_api_url(self):
        return self.href

    def __str__(self):
        return f"BinVO<{self.closet_name}>"


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=20)
    picture_url = models.CharField(max_length=250)
    bin = models.ForeignKey(BinVO, on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_shoe", kwargs={"pk": self.pk})

    def __str__(self):
        return f"Shoe<{self.manufacturer} {self.model_name}>"