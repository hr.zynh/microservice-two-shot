from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVODetailEncodeer(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer"
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"
    ]

    encoders = {
        "bin": BinVODetailEncodeer()
    }