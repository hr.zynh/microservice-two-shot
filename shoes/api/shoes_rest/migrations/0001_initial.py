# Generated by Django 4.0.3 on 2022-10-19 23:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=100)),
                ('href', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=50)),
                ('model_name', models.CharField(max_length=50)),
                ('color', models.CharField(max_length=20)),
                ('picture_url', models.CharField(max_length=250)),
                ('bin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shoes_rest.binvo')),
            ],
        ),
    ]
